    <footer id="footer" role="contentinfo">
      <section class="section swatch-blue-white has-top">
        <div class="decor-top">
          <svg class="decor" height="100%" preserveaspectratio="none" version="1.1" viewbox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 100 Q 2.5 40 5 100 Q 7.5 40 10 100 Q 12.5 40 15 100 Q 17.5 40 20 100 Q 22.5 40 25 100 Q 27.5 40 30 100 Q 32.5 40 35 100 Q 37.5 40 40 100 Q 42.5 40 45 100 Q 47.5 40 50 100 Q 52.5 40 55 100 Q 57.5 40 60 100 Q 62.5 40 65 100 Q 67.5 40 70 100 Q 72.5 40 75 100 Q 77.5 40 80 100 Q 82.5 40 85 100 Q 87.5 40 90 100 Q 92.5 40 95 100 Q 97.5 40 100 100 " stroke-width="0"></path>
          </svg>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center">
              <div id="swatch_social-2" class="sidebar-widget  widget_swatch_social">
                <ul class="unstyled inline small-screen-center social-icons social-background social-big">
                  <li>
                    <a target="_blank" href="https://www.facebook.com/UneMeilleureVieRolePlay">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://twitter.com/umvrp">
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://www.youtube.com/channel/UClYDuAocgDw3z0KZ-QvXZhw">
                      <i class="fa fa-youtube"></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div id="text-4" class="sidebar-widget widget_text">
                <div class="textwidget">Copyright © 2017 Portail UMV-RP - Tous droits réservés
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </footer>
    <script src="assets/js/packages.min.js"></script>
    <script src="assets/js/theme.min.js"></script>
  </body>
</html>