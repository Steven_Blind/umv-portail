<?php include 'header.php' ?>
    <section class="section swatch-blue-white">
      <div class="container">
          <header class="section-header underline">
              <h1 class="headline hyper hairline" style="margin-bottom: 2rem;">F.A.Q</h1>
          </header>
      </div>
    </section>
    <section class="section swatch-white-blue">
      <div class="decor-top">
        <svg class="decor" height="100%" preserveaspectratio="none" version="1.1" viewbox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg">
          <path d="M0 0 C50 100 50 100 100 0  L100 100 0 100" stroke-width="0"></path>
        </svg>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-2  text-default"></div>
          <div class="col-md-8  text-default">
            <div class="panel-group" id="accordion_287">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <a class="accordion-toggle collapsed" data-parent="#accordion_287" data-toggle="collapse" href="#group_1">1. WHY OUR TEAM?</a>
                </div>
                <div class="panel-collapse collapse" id="group_1">
                  <div class="panel-body">
                    <p></p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consequat mattis dolor, commodo ultrices elit posuere non. Proin nec lorem ipsum. Sed tincidunt lorem id sapien</p>
                    <p></p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <a class="accordion-toggle collapsed" data-parent="#accordion_287" data-toggle="collapse" href="#group_2">2. WHY OUR TEAM?</a>
                </div>
                <div class="panel-collapse collapse" id="group_2">
                  <div class="panel-body">
                    <p></p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consequat mattis dolor, commodo ultrices elit posuere non. Proin nec lorem ipsum. Sed tincidunt lorem id sapien</p>
                    <p></p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <a class="accordion-toggle collapsed" data-parent="#accordion_287" data-toggle="collapse" href="#group_3">3. WHY OUR TEAM?</a>
                </div>
                <div class="panel-collapse collapse" id="group_3">
                  <div class="panel-body">
                    <p></p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam consequat mattis dolor, commodo ultrices elit posuere non. Proin nec lorem ipsum. Sed tincidunt lorem id sapien</p>
                    <p></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2  text-default"></div>
        </div>
      </div>
    </section>
<?php include 'footer.php' ?>